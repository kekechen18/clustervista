# ClusterVista

An optimized implementation of Star-Coordinates for interactive visual exploration of clustering structures

The original website is http://www.cc.gatech.edu/projects/disl/VISTA/main.html

This project was built with JetBrains IntelliJ IDEA.