package vdm;

//import eu.hansolo.rangeslider.RangeSlider;

import javax.swing.*;
import javax.swing.border.CompoundBorder;
import javax.swing.border.EmptyBorder;
import javax.swing.border.TitledBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Created by kekechen on 6/18/2014.
 */

public class ClusterDetector extends JFrame{

    DrawPanel dpnl;
    Dimension _screenSize;
    Dimension _dpsize;
    //AlphaSlider _as;
    //ZoomSlider _zs;
    JPopupMenu popup;
    AlphaStepMenuItem _alphaSlider;
    ZoomMenuItem _zoomSlider;
    DimensionRange _dimRange;
    public ClusterDetector(String title) {
        initUI(title);
    }

    public final void initUI(String title) {

        Toolkit tk=getToolkit();

        _screenSize = tk.getScreenSize();
        setTitle(title);
        setSize(_screenSize);
        setBackground(Color.BLACK);
        addMenu();
        dpnl = new DrawPanel();
        _dpsize = new Dimension((int)_screenSize.getWidth()-100, (int)_screenSize.getHeight()-100);
        dpnl.setSize(_dpsize);
        dpnl.setMapping (new Mapping ((float)_dpsize.getWidth()/2-10, (float)_dpsize.getHeight()/2-10, (float)_dpsize.getHeight()/2-5));
        add(dpnl);
        setLocationRelativeTo(null);
        createPopupMenu();
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    }


    public void addMenu(){
        JMenuBar menuBar;
        JMenu menu, submenu;
        JMenuItem menuItem;
        JRadioButtonMenuItem rbMenuItem;
        JCheckBoxMenuItem cbMenuItem;

        ActionListener al = new MenuActionListener();
//Create the menu bar.
        menuBar = new JMenuBar();

//Build the first menu.
        menu = new JMenu("File");
        menuBar.add(menu);

//a group of JMenuItems
        menuItem = new JMenuItem("Open", KeyEvent.VK_O);
        menuItem.getAccessibleContext().setAccessibleDescription(
                "Open a data file");
        menuItem.addActionListener(al);
        menu.add(menuItem);
        menuItem = new JMenuItem("Exit", KeyEvent.VK_E);
        menuItem.getAccessibleContext().setAccessibleDescription(
                "Exit");
        menuItem.addActionListener(al);
        menu.add(menuItem);

//Build the first menu.
        menu = new JMenu("Label");
        menuBar.add(menu);

        menuItem = new JMenuItem("Load Label", KeyEvent.VK_L);
        menuItem.getAccessibleContext().setAccessibleDescription(
                "Load a Label File");
        menuItem.addActionListener(al);
        menu.add(menuItem);

        menuItem = new JMenuItem("Save Label", KeyEvent.VK_S);
        menuItem.getAccessibleContext().setAccessibleDescription(
                "Save the clustering result to a label file");
        menuItem.addActionListener(al);
        menu.add(menuItem);

//Build the first menu.
        menu = new JMenu("Rendering");
        menuBar.add(menu);

        menuItem = new JMenuItem("Load Parameters", KeyEvent.VK_P);
        menuItem.getAccessibleContext().setAccessibleDescription(
                "Load a Label File");
        menuItem.addActionListener(al);
        menu.add(menuItem);

        menuItem = new JMenuItem("Save Parameters", KeyEvent.VK_V);
        menuItem.getAccessibleContext().setAccessibleDescription(
                "Save the clustering result to a label file");
        menuItem.addActionListener(al);
        menu.add(menuItem);

// random rendering
        menu.addSeparator();
        ButtonGroup group = new ButtonGroup();
        rbMenuItem = new JRadioButtonMenuItem("Random Rendering(RR)");
        rbMenuItem.setSelected(false);
        rbMenuItem.setMnemonic(KeyEvent.VK_R);
        rbMenuItem.getAccessibleContext().setAccessibleDescription(
                "Randomly Change Alpha parameters");
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(al);
        menu.add(rbMenuItem);

        rbMenuItem = new JRadioButtonMenuItem("Stop RR");
        rbMenuItem.setMnemonic(KeyEvent.VK_T);
        rbMenuItem.setSelected(true);
        group.add(rbMenuItem);
        rbMenuItem.addActionListener(al);
        menu.add(rbMenuItem);
        setJMenuBar(menuBar);
    }

    class AlphaStepMenuItem extends JSlider implements MenuElement {
        public AlphaStepMenuItem(  ) {
            super(JSlider.HORIZONTAL, 1, 10, 5);
            setBorder(new CompoundBorder(new TitledBorder("AlphaStep"),
                    new EmptyBorder(10, 10, 10, 10)));
            //as.addChangeListener(this);
            setMajorTickSpacing(1);
            setPaintTicks(true);
            addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    JSlider slider = (JSlider) e.getSource();
                    //System.out.println("slider: "+slider.getValue()+ " "+slider.getUpperValue());
                    float stepsize = (float) (slider.getValue() / 100.0);
                    dpnl.updateAlphaStep(stepsize);
                }
            });

        }
        public void processMouseEvent(MouseEvent e, MenuElement path[ ],
                                      MenuSelectionManager manager) {}
        public void processKeyEvent(KeyEvent e, MenuElement path[ ],
                                    MenuSelectionManager manager) {}
        public void menuSelectionChanged(boolean isIncluded) {}
        public MenuElement[ ] getSubElements(  ) {return new MenuElement[0];}
        public Component getComponent(  ) {return this;}
    }

    class ZoomMenuItem extends JSlider implements MenuElement {
        public ZoomMenuItem(  ) {
            super(JSlider.HORIZONTAL, 1, 10, 5);
            setBorder(new CompoundBorder(new TitledBorder("Zoom"),
                    new EmptyBorder(10, 10, 10, 10)));
            //as.addChangeListener(this);
            setMajorTickSpacing(1);
            setPaintTicks(true);
            addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    JSlider source = (JSlider) e.getSource();
                    Mapping map= dpnl.getMapping();
                    if (map!=null) {
                        float v = source.getValue()-5;
                        float z=0;
                        if (v==0)
                            z=1;
                        else {
                            if (v > 0)
                                z = 1 + v * 0.5f;
                            else
                                z = 1 + v * 0.2f;
                        }

                        if (map.getZoom()!=z) {
                            //System.out.println("zoom:" + map.getZoom()+ " " + z + " "+ source.getValue());
                            map.setZoom(z);
                            dpnl.repaint();
                        }
                    }
                }
            });

        }
        public void processMouseEvent(MouseEvent e, MenuElement path[ ],
                                      MenuSelectionManager manager) {
        }
        public void processKeyEvent(KeyEvent e, MenuElement path[ ],
                                    MenuSelectionManager manager) {}
        public void menuSelectionChanged(boolean isIncluded) {}
        public MenuElement[ ] getSubElements(  ) {return new MenuElement[0];}
        public Component getComponent(  ) {return this;}
    }

    class DimensionRange extends JPanel implements MenuElement{
        int _dim;
        RangeSlider rs;
        public DimensionRange(){
            rs = new RangeSlider(0, 20);
            rs.setMajorTickSpacing(1);
            rs.setPaintTicks(true);
            rs.setValue(0);
            rs.setUpperValue(20);
            rs.addChangeListener(new ChangeListener() {
                public void stateChanged(ChangeEvent e) {
                    RangeSlider slider = (RangeSlider) e.getSource();
                    //System.out.println("slider: "+slider.getValue()+ " "+slider.getUpperValue());
                    dpnl.updateRange(_dim, (slider.getValue()-10)/10.0, (slider.getUpperValue()-10)/10.0);
                }
            });
            add(rs);
        }

        void setAxis(Axis a){
            //System.out.println("set axis "+a.getId()+ " "+a.getLower()+" "+a.getUpper());
            _dim =a.getId();
            setBorder(new CompoundBorder(new TitledBorder("Dimension " + _dim + " range"),
                    new EmptyBorder(10, 10, 10, 10)));
            rs.setValue((int) (a.getLower() * 10) + 10);
            rs.setUpperValue((int)(a.getUpper() *10) +10);
        }

        int getValue(){
            return rs.getValue();
        }
        int getUpperValue(){
            return rs.getUpperValue();
        }

        public void processMouseEvent(MouseEvent e, MenuElement path[ ],
                                      MenuSelectionManager manager) {}
        public void processKeyEvent(KeyEvent e, MenuElement path[ ],
                                    MenuSelectionManager manager) {}
        public void menuSelectionChanged(boolean isIncluded) {}
        public MenuElement[ ] getSubElements(  ) {return new MenuElement[0];}
        public Component getComponent(  ) {return this;}

    }

    public void createPopupMenu() {
        JMenuItem menuItem;
        ActionListener al = new MenuActionListener();
        //Create the popup menu.
        popup = new JPopupMenu();
        menuItem = new JMenuItem("Mark Cluster");
        menuItem.addActionListener(al);
        popup.add(menuItem);
        menuItem = new JMenuItem("Remove Cluster");
        menuItem.addActionListener(al);
        popup.add(menuItem);
        menuItem = new JMenuItem("Show Records");
        menuItem.addActionListener(al);
        popup.add(menuItem);
        popup.add(new JSeparator(  ));
        _alphaSlider = new AlphaStepMenuItem(  );
        popup.add(_alphaSlider);
        popup.add(new JSeparator(  ));
        _zoomSlider= new ZoomMenuItem(  );
        popup.add(_zoomSlider);
        _dimRange = new DimensionRange();
        popup.add(_dimRange);
        popup.addPopupMenuListener(new PopupListener());

        //Add listener to the text area so the popup menu can come up.
        //MouseListener popupListener = new PopupListener(popup);
        dpnl.setComponentPopupMenu(popup);
    }

    class PopupListener implements PopupMenuListener {
        public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
            Mapping map = dpnl.getMapping();
            if (map!=null)
                _zoomSlider.setValue(5);
            _alphaSlider.setValue((int)(dpnl.getAlphaStep()*100));

            Axis a = dpnl.getCurrentAxis();
            if (a==null) {
                popup.remove(_dimRange);
            }else{
                _dimRange.setAxis(a);
                popup.add(_dimRange);
            }
        }

        public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
            //dpnl.updateAlphaStep((float) (_alphaSlider.getValue() / 100.0));
        }
        public void popupMenuCanceled(PopupMenuEvent e) {
            //System.out.println("Popup menu is hidden!");
        }
    }

    public static void main(String[] args) {

        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                ClusterDetector ex = new ClusterDetector("VISTA 0.3.2");
                ex.setVisible(true);
            }
        });
    }


    class MenuActionListener implements ActionListener {
        public void actionPerformed(ActionEvent actionEvent) {
            String cmd = actionEvent.getActionCommand();
            System.out.println("Selected: " + cmd);
            if (cmd.equals("Open"))
                openFile();
            else if (cmd.equals("Exit"))
                System.exit(0);
            else if (cmd.equals("Load Label"))
                loadLabel();
            else if (cmd.equals("Save Label"))
                saveLabel();
            else if (cmd.equals("Mark Cluster"))
                dpnl.markCluster();
            else if (cmd.equals("Remove Cluster"))
                dpnl.unmarkCluster();
            else if (cmd.equals("Random Rendering(RR)"))
                dpnl.startRandRendering();
            else if (cmd.equals("Stop RR"))
                dpnl.stopRandRendering();
            else if (cmd.equals("Load Parameters"))
                loadParams();
            else if (cmd.equals("Save Parameters"))
                saveParams();
            else if (cmd.equals("Show Records"))
                dpnl.showRecords();

        }

        private void loadParams(){
            if (dpnl.getDataset()==null){
                JOptionPane.showMessageDialog(null, "Data File has not been loaded. Use \"open\" first");
                return;
            }
            JFileChooser fc = new JFileChooser();
            int returnVal = fc.showOpenDialog(null);
            File f=null;
            if (returnVal != JFileChooser.APPROVE_OPTION) {
                return;
            }
            f = fc.getSelectedFile();
            BufferedReader input;
            String params="";
            try{
                FileReader inputFS = new FileReader(f);
                String line;
                input = new BufferedReader(inputFS);
                while ((line = input.readLine()) != null) {
                    params += line;
                }
                input.close();
            }
            catch(Exception e){
                System.out.println("Can not open file");
                JOptionPane.showMessageDialog(null, "Cannot open file.");
                return;
            }

            dpnl.getMapping().decode(params);
            dpnl.updateAxes();
            dpnl.repaint();
        }



        private void loadLabel(){
            if (dpnl.getDataset()==null){
                JOptionPane.showMessageDialog(null, "Data File has not been loaded. Use \"open\" first");
                return;
            }
            JFileChooser fc = new JFileChooser();
            int returnVal = fc.showOpenDialog(null);
            File f=null;
            if (returnVal != JFileChooser.APPROVE_OPTION) {
                return;
            }
            f = fc.getSelectedFile();
            BufferedReader input;
            Vector<String> v = new Vector<String>();
            try{
                FileReader inputFS = new FileReader(f);
                String line;
                input = new BufferedReader(inputFS);
                while ((line = input.readLine()) != null) {
                    // process the line.
                    String s = line.trim();
                    if(!s.equals(""))
                        v.add(s);
                }
                input.close();
            }
            catch(Exception e){
                System.out.println("Can not open file");
                JOptionPane.showMessageDialog(null, "Cannot open file.");
                return;
            }

            if (v.size()!=dpnl.getRecordSet().size()){
                JOptionPane.showMessageDialog(null, "The length of label file does not match the data file.");
                return;
            }
            int [] label = new int[v.size()];
            for (int i=0; i<v.size();i++){
                label[i] = Integer.valueOf(v.elementAt(i)); // assume zero is the noise label
                //System.out.println(String.valueOf(label[i]));
            }

            dpnl.getClustering().load(label, dpnl.getRecordSet());
            dpnl.repaint();
        }
        private void saveLabel(){
            int [] label = new int[dpnl.getRecordSet().size()];
            for (int i=0; i<label.length; i++){
                label[i]=0; // outliers
            }
            dpnl.getClustering().fillLabel(label);
            JFileChooser fc = new JFileChooser();
            int returnVal = fc.showOpenDialog(null);
            File f=null;
            if (returnVal != JFileChooser.APPROVE_OPTION) {
                return;
            }
            f = fc.getSelectedFile();
            BufferedWriter output;
            try{
                FileWriter outputFS = new FileWriter(f);
                output = new BufferedWriter(outputFS);
                for (int i=0; i<label.length;i++){
                    output.write(String.valueOf(label[i])+"\n");

                }
                output.close();
                outputFS.close();
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, "Cannot open file.");
            }

        }


        private void saveParams(){
            String params = dpnl.getMapping().encode();

            JFileChooser fc = new JFileChooser();
            int returnVal = fc.showOpenDialog(null);
            File f=null;
            if (returnVal != JFileChooser.APPROVE_OPTION) {
                return;
            }
            f = fc.getSelectedFile();
            BufferedWriter output;
            try{
                FileWriter outputFS = new FileWriter(f);
                output = new BufferedWriter(outputFS);
                output.write(params);
                output.close();
                outputFS.close();
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, "Cannot open file.");
            }

        }

        private void openFile(){
            if (dpnl.loadData()==false){
                System.out.println("load failed...");
                return;
            }else{
                setTitle("VISTA 0.3.2 - "+dpnl.getDataset().getFilename());
            }
        }

    }
/*
    class AlphaSlider extends JPanel implements ChangeListener {
        public AlphaSlider(){
            super();
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            JLabel al = new JLabel("AlphaStep");
            add(al);
            JSlider as = new JSlider(JSlider.VERTICAL, 1, 10, 5);
            as.addChangeListener(this);
            as.setMajorTickSpacing(1);
            as.setPaintTicks(true);
            add(as);
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            JSlider source = (JSlider)e.getSource();
            if (!source.getValueIsAdjusting()) {
                float stepsize = (float) (source.getValue()/100.0);
                dpnl.updateAlphaStep(stepsize);
            }
        }
    }


    class ZoomSlider extends JPanel implements ChangeListener {
        int _resol;
        public ZoomSlider(){
            super();
            setLayout(new BoxLayout(this, BoxLayout.Y_AXIS));
            JLabel al = new JLabel("Zoom");
            add(al);
            JSlider as = new JSlider(JSlider.VERTICAL, 1, 10, 4);
            as.addChangeListener(this);
            as.setMajorTickSpacing(1);
            as.setPaintTicks(true);
            add(as);
            _resol=200;
        }
        public int getResol(){
            return _resol;
        }

        @Override
        public void stateChanged(ChangeEvent e) {
            JSlider source = (JSlider)e.getSource();
            if (!source.getValueIsAdjusting()) {
                float stepsize = (float)(source.getValue()/100.0);
                dpnl.updateAlphaStep(stepsize);
            }
        }
    }
*/
}
