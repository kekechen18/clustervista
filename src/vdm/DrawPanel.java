/**
	Terms of Use
	
	VISTA - Visual Cluster Rendering System	
	Copyright (c) 2002-2005 Keke Chen <kekechen at cc.gatech.edu>, DISL lab, Georgia Tech
	
	This program is free for non-commercial use; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by the Free Software 
	Foundation.
	
	If you use this software in your research, please acknowledge VISTA visual cluster 
	rendering system and its authors, and link to back to the project page 
	http://disl.cc.gatech.edu/VISTA/.
	
	Please cite VISTA in academic publications as: 
	
	Keke Chen and Ling Liu. VISTA: Validating and Refining Clusters via Visualization. 
	Journal of Information Visualization. Vol3, Issue4, Sept. 2004. 
	
*/
	
	
/**
 * DrawPanel.java
 *
 *
 * Created: Tue Jun 26 11:08:45 2001
 *
 * @author: Keke Chen, kekechen at cc.gatech.edu
 * @version 0.2.1
 */

package vdm;


import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;
import java.math.*;
import java.util.Timer;

public class DrawPanel extends JPanel implements MouseListener, MouseMotionListener{
   Axis [] _axis;
   Clustering _cs; // visual clustering result
   RecordSet _rs; // visualized points
    Vector<Point> _mouseDrag;
   AutoRender _ar;
   Timer _timer, _timer2;
    Mapping _map;
    boolean _inAxis;
    RecordSet _selected; //
    Point _current;
    Dataset _ds; // raw data from disk
    int _rightClickAxis;
    public DrawPanel (){
        _selected = new RecordSet(Record.SELECTED_ID);
        _map = null;

        _rs = null;
        _ds = new Dataset();
        _cs = new Clustering();
        _axis = null;
        _timer=null;
        _rightClickAxis = -1;
       _mouseDrag = new Vector<Point>();
       setBackground(new Color(186,230,244));
        addMouseListener(this);
        addMouseMotionListener(this);

   }

    public Mapping getMapping(){
        return _map;
    }
    public void setMapping (Mapping m){
        _map = m;
    }

    public void clear(){
        _rs = null;
        _cs.clear();
        _axis = null;
        repaint();
    }
    public boolean loadData(File f){

        _rs = _ds.load2(0, false, f, _map);
        if (_rs==null){
            return false;
        }
        else {
            _map.init(_ds.getDim());
            createAxes();
            //System.out.println(_ds.getDim() + " dims "+ _rs.size() + "rows. request repaint");
            repaint();
            return true;
        }
    }


    public Axis getCurrentAxis(){
        if (_rightClickAxis==-1)
            return null;
        return _axis[_rightClickAxis];
    }

    public void updateRange(int dim, double lower, double upper){
        _axis[dim].setRange(lower, upper);
        double [] lowers = new double[_axis.length];
        double [] highs = new double[_axis.length];
        for (int i=0; i<_axis.length; i++){
            lowers[i] = _axis[i].getLower();
            highs[i] = _axis[i].getUpper();
        }
        _rs.filterByRange(lowers, highs, _selected);
        repaint();
    }

    public boolean loadLabel(String f){
        BufferedReader input;
        Vector<String> v = new Vector<String>();
        try{
            FileReader inputFS = new FileReader(f);
            String line;
            input = new BufferedReader(inputFS);
            while ((line = input.readLine()) != null) {
                // process the line.
                String s = line.trim();
                if(!s.equals(""))
                    v.add(s);
            }
            input.close();
        }
        catch(Exception e){
            e.printStackTrace();
            //JOptionPane.showMessageDialog(null, "Cannot open file.");
            return false;
        }

        if (v.size()!=_rs.size()){
            System.err.println("label file size does not match");
            //JOptionPane.showMessageDialog(null, "The length of label file does not match the data file.");
            return false;
        }
        int [] label = new int[v.size()];
        for (int i=0; i<v.size();i++){
            label[i] = Integer.valueOf(v.elementAt(i)); // assume zero is the noise label
            //System.out.println(String.valueOf(label[i]));
        }

        _cs.load(label, _rs);
        repaint();
        return true;
    }

    public boolean loadData(){
        _rs = _ds.load(_map);
        if (_rs==null)
            return false;
        else {
            _map.init(_ds.getDim());
            createAxes();
            System.out.println("request repaint");
            repaint();
            return true;
        }
    }

    public RecordSet getRecordSet(){
        return _rs;
    }
    public Clustering getClustering(){
        return _cs;
    }
    public Dataset getDataset(){
        return _ds;
    }
    public void markCluster(){
        if (_selected.size()==0)
            return;
        RecordSet rs = _cs.newCluster();
        _selected.dump(rs);
        System.err.println("mark cluster " + rs.getID());
        repaint();
    }
    public void unmarkCluster(){
        Integer idx = _cs.findCluster(_current);
        if (idx!=null) {
            System.err.println("find cluster "+idx.toString());
            _cs.removeCluster(idx);
            repaint();
        }
    }

    public void mouseClicked(java.awt.event.MouseEvent evt) {
        if (_rs==null)
            return;
      }

    public void mousePressed(java.awt.event.MouseEvent evt) {
        if (_rs==null)
            return;
        if (evt.getButton()!= MouseEvent.BUTTON1) {
            // right click
            if (_axis != null) {
                _rightClickAxis=-1;
                for (int i = 0; i < _axis.length; i++) {
                    if (_axis[i].contains(evt.getPoint())) {
                        _rightClickAxis = i;
                        //System.out.println("right clicked axis" + i);
                    }
                }

            }

            return;
        }

        _inAxis = false;
        if (_axis != null) {
            for (int i = 0; i < _axis.length; i++) {
                if (_axis[i].contains(evt.getPoint())) {
                    _timer = new Timer(true);
                    _timer.scheduleAtFixedRate(new AutoRender(_axis[i]), 0, 100);
                    _inAxis = true;
                }
            }

        }


        _mouseDrag.clear();
        if (!_inAxis){
            _mouseDrag.add(evt.getPoint());
        }
    }

    public void mouseReleased(java.awt.event.MouseEvent evt) {
        if (_rs==null)
            return;

        if (_timer!=null) {
            _timer.cancel();
            _timer = null;
            if (_mouseDrag.size()>1){
                _mouseDrag.clear();
                repaint();
            }
        }
        if (!_inAxis) {
            Point p = _mouseDrag.elementAt(0);
            _mouseDrag.addElement(p);
            AreaScan as = new AreaScan(_mouseDrag,_rs);
            as.pickSubset(_selected);
            repaint();
        }

    }

    public void mouseExited(java.awt.event.MouseEvent evt) {
    }
    public void mouseEntered(java.awt.event.MouseEvent evt) {
    }

    public void mouseMoved(java.awt.event.MouseEvent evt) {
        _current = evt.getPoint();
        /*
        ClusterMap cm = ClusterMap.getInstance();
        Vector<Integer> r = cm.get(_current);
        if (r==null)
            return;


        Dataset d = Dataset.getInstance();
        String txt = "";
        for (Integer i: r){
            txt += "ID"+i.toString()+":"+d.getOrigRecord(i.intValue());
        }
        setToolTipText(txt);
        System.out.println(txt);*/
    }
    public void showRecords(){
        //ClusterMap cm = ClusterMap.getInstance();
        //Vector<Integer> r = cm.get(_current.x, _current.y);
        //System.err.println(r);
        //if (r.isEmpty())
         //   return;


        String txt = "In range x["+
                String.valueOf(_current.x-10)+","+
                String.valueOf(_current.x+10)+"], y["+
                String.valueOf(_current.y-10)+","+
                String.valueOf(_current.y+10)+"]\n";
        /*
        for (Integer i: r){
            txt += "ID"+i.toString()+": "+_ds.getOrigRecord(i.intValue())+"\n";
        }
    */
        //setToolTipText(txt);
        //System.out.println(txt);
        JOptionPane.showMessageDialog(null, txt, "Records on Screen", JOptionPane.PLAIN_MESSAGE);
    }
    public Point getCurrentMouse(){
        return _current;
    }
    public void mouseDragged(java.awt.event.MouseEvent evt) {
        if (_rs==null)
            return;
        if (!_inAxis) {
            _mouseDrag.add(evt.getPoint());
            repaint();
        }
    }

    public void startRandRendering(){
        _timer2 = new Timer(true);
        _timer2.scheduleAtFixedRate(new AutoRenderAll(_axis), 0, 100);
    }

    public void stopRandRendering(){
        if (_timer2==null)
            return;
        _timer2.cancel();
        _timer2 = null;
    }

@Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        doDrawing(g);
    }

    private void doDrawing(Graphics g) {
        //System.out.println("drawing...");
        if (_rs==null) // no data loaded
            return;

        if (_axis!=null){
            for (Axis a: _axis){
                a.draw(g);
            }
        }

        if (_rs!=null) {
            if (_map.updated()) {
                //ClusterMap cm = ClusterMap.getInstance();
                //cm.clear();
                _rs.trans();
                _map.resetUpdate();
            }
            _rs.draw(g);
            //System.out.println("drawing rs");
        }

        if (_mouseDrag.size()>1){
            Point cp=_mouseDrag.elementAt(0), pp=_mouseDrag.elementAt(1);
            g.setColor(Color.RED);
            for (int i=1; i<_mouseDrag.size(); i++){
                pp = _mouseDrag.elementAt(i);
                g.drawLine(cp.x, cp.y, pp.x, pp.y);
                cp = pp;
            }
        }
    }

    public float getAlphaStep(){
        if (_map!=null)
                return _map.getAlphaStep();
        return 0f;
    }

    public void updateAlphaStep(float s){
        if (_map==null || _axis == null)
            return;
        for (Axis a: _axis)
            a.setSteplen(s);
        _map.setAlphaStep(s);
    }
    public void updateAxes(){
        // with updated values in Mapping
        if (_map==null || _axis==null)
            return;

        float [] as = _map.getAlphas();

        for (int i=0; i<_axis.length; i++){
            _axis[i].setAlpha(as[i]);
        }
    }
    /*
    public void addDataset(RecordSet rs, Mapping mp){
        _rs = rs;
        _map = mp;
        _cs.clear();
        repaint();
    }*/
   public void createAxes(){
       if (_ds==null)
           return;

      int num  = _ds.getDim();
      String [] name = _ds.getColumnNames();
      _axis=new Axis [num];

      Dimension dm = getSize();
      float r= (float)(dm.getHeight()/2.0-10);
      int origx = getX();
      int origy = getY();
      int width = (int) dm.getWidth()/2;
      int height = (int) dm.getHeight()/2;
       Point orig = new Point(origx + width , origy + height);
       Point p;

      if (num==2) { // if only 2d, one d on 0degree, the other on PI/2 degree
		 p=new Point((int)(orig.getX()+r*Math.cos(Math.PI/6)),
			     (int)(orig.getY()-r*Math.sin(Math.PI/6))); // 0 degree
		 _axis[0]=new Axis(0,name[0], orig, p, _map.getMaxAlpha(), _map.getMinAlpha(), _map);
	
		 p=new Point((int)(orig.getX()+r*Math.cos(Math.PI*2/3)),
			     (int)(orig.getY()-r*Math.sin(Math.PI*2/3)));// PI/2 degree
          _axis[1]=new Axis(1,name[1], orig, p, _map.getMaxAlpha(), _map.getMinAlpha(), _map);
	
		 return;

      } // end of if ()
      
      for ( int i=0;i<num;i++) { // equally dividing circle
		 p=new Point((int)(orig.getX()+r*Math.cos(i*2*Math.PI/num+Math.PI/6)),
			     (int)(orig.getY()-r*Math.sin(i*2*Math.PI/num+Math.PI/6)));

          _axis[i]=new Axis(i,name[i], orig, p, _map.getMaxAlpha(), _map.getMinAlpha(), _map);

      } // end of for ()
      
   }


    public class AutoRender extends TimerTask {
        Axis _a;
        public AutoRender(Axis a){
            _a = a;
        }

        @Override
        public void run() {
            _a.autoChange();
            repaint();
        }
    }

    public class AutoRenderAll extends TimerTask {
        Axis [] _as;
        public AutoRenderAll(Axis [] as){
            _as = as;
        }

        @Override
        public void run() {
            for (Axis a: _as)
                a.randomChange();
            repaint();
        }
    }

}// DrawPanel

