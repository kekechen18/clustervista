package vdm;
import org.json.*;
import org.json.JSONArray;
import org.json.JSONObject;

/**
 * Created by kekechen on 6/18/2014.
 */
public class Mapping {
    float _resol, _shiftx, _shifty, _center;
    float [] _alpha;
    float [] _theta;
    float [] _paramx;
    float [] _paramy;
    int _dim;
    boolean _update;
    float _maxAlpha, _minAlpha;
    float _alphaStep;

    //public static final Mapping _instance = new Mapping();
    public Mapping (float shiftx, float shifty, float center){
        // initial setting
        _alphaStep = 0.05f;
        _minAlpha = -1f;
        _maxAlpha = 1f;
        _resol = 1f;
        _shiftx = shiftx;
        _shifty = shifty;
        _center =center;

    }
    //public static Mapping getInstance(){
    //    return _instance;
    //}
    public float [] getAlphas(){
        return _alpha;
    }
    public float getZoom(){
        return _resol;
    }
    public float getAlphaStep(){
        return _alphaStep;
    }
    public void setAlphaStep(float v){
        _alphaStep = v;
    }
    public float getMaxAlpha(){
        return _maxAlpha;
    }
    public float getMinAlpha(){
        return _minAlpha;
    }

    public void setAlphaRange(float mina, float maxa){
        _minAlpha = mina;
        _maxAlpha = maxa;
    }

    public float [] getThetas(){
        return _theta;
    }

    public void init(int dim) {

        _alpha = new float[dim];
        _theta = new float[dim];
        _paramx = new float[dim];
        _paramy = new float[dim];
        _dim = dim;
        for (int i = 0; i < _dim; i++) {
            _theta[i] = (float) (i*2*Math.PI/_dim+Math.PI/6);
            _alpha[i] = 0.5f;
        }
        if (_dim==2){
            _theta[0]=(float)Math.PI/6;
            _theta[1] = (float) Math.PI*2/3;
        }
        updateParam();
    }

    private void updateParam(){
        //System.out.println("Mapping:: update params; dim="+_dim+ " resol"+_resol+" center"+_center+" shiftx"+_shiftx);

        for (int i=0; i<_dim; i++) {
            _paramx[i]= (_alpha[i]*(float)Math.cos(_theta[i])*_resol*_center*2/_dim);
            _paramy[i]= -_alpha[i]*(float)Math.sin(_theta[i])*_resol*_center*2/_dim;
            //System.out.println(String.valueOf(_paramx[i])+ " "+ String.valueOf(_paramy[i]));
        } // end of for ()
        _update=true;
    }

    public void resetUpdate(){
        _update=false;
    }

    public boolean updated(){
        return _update;
    }

    public float[] getParamx(){
        return _paramx;
    }
    public float getShiftx(){
        return _shiftx;
    }
    public float getShifty(){
        return _shifty;
    }
    public float[] getParamy(){
        return _paramy;
    }

    public void setZoom(float v){
        _resol = v;
        updateParam();
        //System.out.println("Zoom is changed");
    }

    public void setAlpha(int i, float a){
        _alpha[i] = a;
        updateParam();
        //System.out.println("Alpha is changed");

    }

    public void setAlphas (float [] a){
        for (int i=0; i< _dim; i++)
            _alpha[i] = a[i];
        updateParam();
        //System.out.println("Alphas are changed");

    }

    public String encode(){
        try {
            JSONObject obj = new JSONObject();
            obj.put("alpha", _alpha);
            obj.put("resol", _resol);
            obj.put("shiftx", _shiftx);
            obj.put("shifty", _shifty);
            obj.put("center", _center);
            obj.put("theta", _theta);
            obj.put("dim", _dim);
            return obj.toString();
        }catch(Exception e){
            return null;
        }
    }
    public void decode(String js){
        JSONObject ji;
        JSONArray nalpha,ntheta;
        try{
            ji= new JSONObject(js);
            _dim = ji.getInt("dim");
            _shiftx = (float) ji.getDouble("shiftx");
            _shifty = (float)ji.getDouble("shifty");
            _resol = (float)ji.getDouble("resol");
            nalpha = ji.getJSONArray("alpha");
            _center =(float) ji.getDouble("center");
            ntheta = ji.getJSONArray("theta");
            for (int i=0; i<_dim; i++){
                _alpha[i] = (float) nalpha.getDouble(i);
                _theta[i] = (float) ntheta.getDouble(i);
            }
        }catch(Exception e){
            System.err.println("necessary parameters are not set");
            System.exit(1);
        }
        //System.out.println("dim"+String.valueOf(_dim) + "_center"+String.valueOf(_center)+"alpha"+String.valueOf(_alpha[1]));
        updateParam();
    }

}
