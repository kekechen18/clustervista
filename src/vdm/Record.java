package vdm;

import java.awt.*;

/**
 * Created by kekechen on 6/18/2014.
 */
public class Record {
    float [] _vals;
    int _dim;
    int _x, _y;
    int _label;
    int _rid;
    int _pointSize;
    Color [] _clrIndex;
    Mapping _map;
    static public final int SELECTED_ID=16;
    static public final int UNSELECTED_ID=0;
    static public final int MAX_CLUSTER=15;

    public Record(int id, int dim, float [] values, Mapping m){
        _map =m;
        _rid = id;
        _dim = dim;
        _vals = values;
        _label =-1; // no label
        _x=_y=0;
        _pointSize = 6;
        _clrIndex = new Color[17]; // maximum 16 colors
        _clrIndex[0]=Color.darkGray; // reserved for outliers
        _clrIndex[1]=Color.pink;
        _clrIndex[2]=Color.blue;
        _clrIndex[3]=new Color ( 132, 22, 98);
        _clrIndex[4]=new Color( 12, 200, 100);
        _clrIndex[5]=Color.magenta;
        _clrIndex[6]=new Color(178,186,4);
        _clrIndex[7]=new Color(193,247,115);
        _clrIndex[8]=new Color(3,156,189);
        _clrIndex[9]=new Color(73,11,91);
        _clrIndex[10]=new Color(248,104,162);
        _clrIndex[11]=new Color(10,87,104);
        _clrIndex[12]=new Color(76,115,9);
        _clrIndex[13]=new Color(115,66,9);
        _clrIndex[14]=new Color(10,92,104);
        _clrIndex[15]=new Color(111,5,255);
        _clrIndex[16]=Color.red; // for temporary markup

    }
/*    public Record(int dim){
        // zero record
        _rid = -1;
        _dim = dim;
        _vals = new double[dim];
        for (int i=0; i<dim; i++)
            _vals[i] = 0;
        _label =-1; // no label
        _x=_y=0;
    }
*/
    public int getID(){
        return _rid;
    }

    public float [] getV(){
        return _vals;
    }

    public void trans(){
        float x = _map.getShiftx();
        float y = _map.getShifty();
        float []paramx = _map.getParamx();
        float []paramy = _map.getParamy();

        for (int j=0; j<_dim; j++) {
            x += paramx[j] * _vals[j];
            y += paramy[j] * _vals[j];
            //System.out.println(paramx[j]+" "+paramy[j]);
        }
        _x = (int) x;
        _y = (int) y;

        //ClusterMap cm = ClusterMap.getInstance();
        //cm.put(_x,_y, _rid);
    }

    public void label(int type){
        _label = type;
    }

    public int getX(){

        return _x;
    }
    public int getY()
    {
        return _y;
    }

    public void setSize(int s){
        _pointSize =s;
    }
    public void draw(Graphics g){
        // size and shape, shape might be determined by the label
        //Graphics2D g2 = (Graphics2D) g;
        //System.out.println("drawing record");

        g.setColor(_clrIndex[_label]);
        int d;
        int pointSize = _pointSize;
        int x =_x, y=_y;

        switch (_label){
            case 1: // cluster 1, use triangle
                d = pointSize/2;
                g.drawRect(_x,_y, pointSize, pointSize);
                /*g.drawLine(x,y-d, x-d, y+d);
                g.drawLine(x-d, y+d, x+d, y+d);
                g.drawLine(x+d, y+d, x, y-d);*/
                break;

            case 3: // cluster 3, use counter-triangle
                d = pointSize/2;
                g.drawLine(x-d,y-d, x, y+d);
                g.drawLine(x, y+d, x+d, y-d);
                g.drawLine(x+d, y-d, x-d, y-d);
                break;

            case 2: // cluster 2, use cross 1
                d = pointSize/2;

                g.drawLine(x-d,y-d, x+d, y+d);
                g.drawLine(x-d, y+d, x+d, y-d);
                break;

            case 4: // cluster 4, use cross 2
                d = pointSize/2;

                g.drawLine(x-d,y, x+d, y);
                g.drawLine(x, y+d, x, y-d);
                break;

            case 5: // cluster 5, use circle
                g.drawOval(_x, _y, pointSize, pointSize);
                break;

            case 6: // cluster 6, use ellipse 1
                g.drawOval(_x, _y, pointSize*3+2, pointSize);
                break;
            case 7: // cluster 7, use ellipse 2
                g.drawOval(_x, _y, pointSize, pointSize*3+2);
                break;

            // unclustered items
            case 0: // noise, square, size
                g.drawRect(_x,_y, pointSize, pointSize);
                break;
            default:
                g.drawRect(_x,_y, pointSize, pointSize);

        }

    }

    public void print(){
        String s ="";
        for (float v: _vals){
            s+=String.valueOf(v)+",";
        }
        System.out.println(String.valueOf(_rid) + " "+String.valueOf(_x) + ","+ String.valueOf(_y)+" "+s);

    }
    public void add(Record r){
        float [] v = r.getV();
        for (int i=0; i<v.length;i++){
            _vals[i] += v[i];
        }
    }

    public void div(double v){
        for (int i=0; i<_vals.length;i++){
            _vals[i] /= v;
        }

    }
}
