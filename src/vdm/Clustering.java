package vdm;

/**
 * Created by kekechen on 6/18/2014.
 */
import javax.swing.*;
import java.awt.*;
import java.util.*;

public class Clustering {
    HashMap<Integer, RecordSet> _cs;
    int _cnum;
    HashSet<Integer> _CIDs;

    public Clustering(){
        _CIDs = new HashSet<Integer>();
        for (int i=1; i<=Record.MAX_CLUSTER; i++)
            _CIDs.add(i);
        _cs = new HashMap<Integer, RecordSet>();
    }

    public RecordSet newCluster(){
        if (_CIDs.isEmpty()){
            JOptionPane.showMessageDialog(null, "Cannot mark more than"+String.valueOf(Record.MAX_CLUSTER));
            return null;
        }
        Integer cnum=null;
        for (Integer c: _CIDs){
            cnum =c;
            break;
        }

        _CIDs.remove(cnum);
        RecordSet rs = new RecordSet(cnum.intValue());
        _cs.put(cnum, rs);
        return rs;
    }

    public void clear(){
        for (RecordSet s: _cs.values()){
            s.clear();
        }
        _cs.clear();
        for (int i=1; i<=Record.MAX_CLUSTER; i++)
            _CIDs.add(i);
    }

    public void removeCluster (Integer i){
        if (_cs.containsKey(i)){
            RecordSet r = _cs.get(i);
            r.clear();
            _cs.remove(i);
            _CIDs.add(i);
        }
    }

    public Integer findCluster(Point p){
        for (Integer i: _cs.keySet()){
            if (_cs.get(i).contains(p)){
                return i;
            }
        }
        return null;
    }

    public void load(int[] label, RecordSet rs){
        clear();
        // use label to split the recordset
        HashMap<Integer, Vector<Integer> > labs = new HashMap<Integer, Vector<Integer> >();
        int maxl=-1;
        for (int i: label){
            if (maxl <i)
               maxl =i;
        }
        for (int i=1; i<=maxl; i++){
            labs.put(i, new Vector<Integer>());
        }

        for (int i=0; i<label.length; i++){
            if (label[i]==0)
                continue;
            Vector<Integer> v =labs.get(label[i]);
            v.add(i);
        }

        for (Vector<Integer> lab: labs.values() ){
            RecordSet rs1 = newCluster();
            rs.fetch(lab, rs1);
            //System.out.println("cluster"+String.valueOf(rs.getID())+" "+String.valueOf(rs.size()));
        }
    }

    public void fillLabel(int [] label){
        // return labels
        for (RecordSet s: _cs.values()){
            s.fillLabel(label);
        }
    }

    public void draw(Graphics g){

        for (RecordSet s: _cs.values())
            s.draw(g);
    }
}
