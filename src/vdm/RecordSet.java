package vdm;

/**
 * Created by kekechen on 6/18/2014.
 */

import java.awt.*;
import java.util.*;
public class RecordSet {
    HashMap<Integer, Record> _rset;
    int _sid;
    int [] _x;
    int [] _y;
    int [] _index;
    int _current;

    public RecordSet(int id){
        _sid = id;
        _rset = new HashMap<Integer, Record>();
        clear();
    }
    public void clear(){
        for (Record r: _rset.values()){
            r.label(Record.UNSELECTED_ID);
        }
        _rset.clear();
        _x =null;
        _y = null;
        _index=null;
        _current=0;
    }

    public int getID(){
        return _sid;
    }

    public int size(){
        return _rset.size();
    }
    public void trans(){
        for (Record r: _rset.values()){
            r.trans();
        }
    }
    public void add(Record r){
        _rset.put(new Integer(r.getID()), r);
        r.label(_sid);
    }

    public boolean contains(Point p){
        collectXY();
        int maxx=-1, maxy=-1, minx=100000, miny=100000;
        for (int i=0; i<size(); i++){
            if (maxx<_x[i])
                maxx=_x[i];
            if (maxy<_y[i])
                maxy= _y[i];
            if (minx>_x[i])
                minx=_x[i];
            if (miny >_y[i])
                miny=_y[i];
        }
        return p.x>minx && p.x<maxx && p.y>miny && p.y<maxy;
    }

    public void fetch(Vector<Integer> rids, RecordSet rs){
        for (Integer i: rids){
            Record r=_rset.get(i);
            rs.add(r);
        }
    }

    public void filterByRange(double [] lows, double [] highs, RecordSet rs){
        rs.clear();
        float [] vs;
        boolean include;
        for (Record r: _rset.values()){
            vs = r.getV();
            include = true;
            //System.out.println("length" + vs.length + " "+lows.length + " "+ highs.length);
            //r.print();

            for(int i=0; i<vs.length; i++)
               if (vs[i] <lows[i] || vs[i]>highs[i]) {
                   include = false;
                   break;
               }
            if (include)
                rs.add(r);
        }
    }

    public void dump(RecordSet rs){
        // copy records to rs, and then clear _rset, for special use
        for (Record r: _rset.values()){
            rs.add(r);
        }
        _rset.clear();
    }
    public void fillLabel(int [] label){
        for (Integer i: _rset.keySet()){
            label[i.intValue()] = _sid;
            //System.out.println("label"+i.toString()+":"+String.valueOf(_sid));
        }
    }

    public int maxID(){
        int maxv=-1;
        for (Integer i: _rset.keySet()){
            int v = i.intValue();
            if (maxv< v)
                maxv=v;
        }
        return maxv;
    }
    public void draw(Graphics g){
        //System.out.println("drawing set");
        for (Record r: _rset.values()){
            r.draw(g);
            //r.print();
        }
    }

    public Record mean(){
        Record r = null;
        for (Record i: _rset.values()){
            if (r==null)
                r = i;
            else
                r.add(i);
        }
        if (_rset.size()>0) {
            r.div(_rset.size());
            return r;
        }
        else
            return null;
    }


    public void collectXY(){
        int size = _rset.size();
        _x = new int[size];
        _y = new int[size];
        _index = new int[size];
        int c=0;
        for (Record r: _rset.values()){
            _x[c] = r.getX();
            _y[c] = r.getY();
            _index[c] = r.getID();
            c++;
        }
    }

    public void sortOnY(){
        collectXY();
        randomizedQuickSort(0, size()-1);
    }

    private void randomizedQuickSort(int p, int r){
        if ( p<r) {
            int q=randomizedPartition(p,r);
            randomizedQuickSort(p,q);
            randomizedQuickSort(q+1,r);
        } // end of if ()

    }


    private int randomizedPartition(int p, int r){
        int t=(int)(Math.random()*(r-p)+p);

        //System.out.println("random value "+t);

        swap(t,p);

        int x= _x[p], y=_y[p];
        int i=p-1, j=r+1;

        while (true) {

            do j--;
            while (_y[j]<y || (_y[j]==y&&_x[j]>x));

            do i++;
            while (_y[i]>y || (_y[i]==y&&_x[i]<x));

            if (i<j)
                swap(i, j);
            else
                return j;

        } // end of while ()


    }

    private void swap(int i, int p){
        int temp;
        int temp1;

        temp=_x[i];
        _x[i]=_x[p];
        _x[p]=temp;

        temp=_y[i];
        _y[i]=_y[p];
        _y[p]=temp;

        temp1=_index[i];
        _index[i]=_index[p];
        _index[p]=temp1;
    }

    public boolean atEnd() {
        return _current>=_rset.size();
    }

    public int getXv(){
        return (int)_x[_current];
    }

    public int getYv(){
        //System.out.println("y="+_y[_current]+"current="+_current+"row="+_row);

        return (int)_y[_current];
    }

    public int getIndex(){
        return _index[_current];
    }

    public void reset() {
        _current=0;
    }

    public int next(){
        _current++;
        if (_current>= _rset.size()) {
            return -1;
        } // end of if ()
        else {

            return 0;
        } // end of else

    }

}
