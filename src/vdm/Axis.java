/**
 Terms of Use

 VISTA - Visual Cluster Rendering System
 Copyright (c) 2002-2005 Keke Chen <kekechen at cc.gatech.edu>, DISL lab, Georgia Tech

 This program is free for non-commercial use; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by the Free Software
 Foundation.

 If you use this software in your research, please acknowledge VISTA visual cluster
 rendering system and its authors, and link to back to the project page
 http://disl.cc.gatech.edu/VISTA/.

 Please cite VISTA in academic publications as:

 Keke Chen and Ling Liu. VISTA: Validating and Refining Clusters via Visualization.
 Journal of Information Visualization. Vol3, Issue4, Sept. 2004.

 */


/**
 * Axis.java
 *
 *
 * Created: Tue Jun 26 22:05:08 2001
 *
 * @author: Keke Chen, kekechen at cc.gatech.edu
 * @version 0.2.1
 */
package vdm;

import java.awt.*;
import java.awt.geom.Ellipse2D;

public class Axis {
    Point _pos, _orig;
    float _alpha, _steplen;
    int _id;
    String _name;
    float _maxAlpha, _minAlpha;
    double _lower, _upper;
    boolean _dec;
    Mapping _map;
    public Axis(int id, String name, Point orig, Point pos, float maxa, float mina, Mapping map) {
        _id = id;
        _maxAlpha = maxa;
        _minAlpha = mina;
        _lower =-1;
        _upper = 1;
        _orig = orig;
        _pos = pos;
        _map = map;

        if (name == null)
            _name = Integer.toString(id);
        else
            _name = name;

        // text color _name.setColor(new Color(40,116,254));

        // end color setColor(new Color(40,116,254));

        _alpha = 0.5f; // initial alpha
        _steplen = 0.05f;
        _dec = false;
    }

    public void setRange(double low, double high) {
        if (low>high)
            return;
        _lower = low;
        _upper = high;
        //System.out.println("Axis "+ _id + " lower "+_lower + " upper "+_upper );
    }
    public double getLower(){
        return _lower;
    }
    public double getUpper(){
        return _upper;
    }

    public void setAlpha(float a){
        _alpha=a;
        _map.setAlpha(_id, _alpha);
    }

    public void setSteplen(float s) {
        _steplen = s;
    }

    public boolean contains(Point p) {
        if (p.x > _pos.x - 20 && p.x < _pos.x + 20 && p.y > _pos.y - 20 && p.y < _pos.y + 20)
            return true;
        else
            return false;
    }

    public void incAlpha() {

        _alpha += _steplen;

        if (_alpha > _maxAlpha)
            _alpha = _maxAlpha;
        _map.setAlpha(_id, _alpha);
    }

    public void decAlpha() {
        _alpha -= _steplen;

        if (_alpha < _minAlpha)
            _alpha = _minAlpha;
        _map.setAlpha(_id, _alpha);

    }

    public void autoChange() {
        if (_dec)
            _alpha -= _steplen;
        else
            _alpha += _steplen;

        if (_alpha < _minAlpha) {
            _alpha = _minAlpha;
            _dec = false;
        } else if (_alpha > _maxAlpha) {
            _alpha = _maxAlpha;
            _dec = true;
        }
        _map.setAlpha(_id, _alpha);

    }

    public void randomChange() {
        if(Math.random()<0.5)
            _dec = true;
        else
            _dec =false;

        if (_dec)
            _alpha -= _steplen;
        else
            _alpha += _steplen;

        if (_alpha < _minAlpha) {
            _alpha = _minAlpha;
            _dec = false;
        } else if (_alpha > _maxAlpha) {
            _alpha = _maxAlpha;
            _dec = true;
        }
        _map.setAlpha(_id, _alpha);

    }

    public float getAlpha() {
        return _alpha;
    }

    public int getId() {
        return _id;
    }

    public void draw(Graphics g) {
        Graphics2D g2 = (Graphics2D) g;
        Point p = new Point();
        if (_alpha > 0) {
            g2.setColor(Color.GREEN);
            p.x = (int) (_alpha / _maxAlpha * (_pos.x - _orig.x) + _orig.x);
            p.y = (int) (_alpha / _maxAlpha * (_pos.y - _orig.y) + _orig.y);
        } else {
            g2.setColor(Color.YELLOW);
            p.x = (int) (_alpha / _minAlpha * (_pos.x - _orig.x) + _orig.x);
            p.y = (int) (_alpha / _minAlpha * (_pos.y - _orig.y) + _orig.y);
        }


        g2.drawLine(_orig.x, _orig.y, p.x, p.y);
        g2.setColor(new Color(40, 116, 254));
        g2.fill(new Ellipse2D.Float(_pos.x, _pos.y, 15, 15));
        g.drawString(_name, _pos.x + 20, _pos.y + 20);
    }
    /*
   public void mouseReleased(java.awt.event.MouseEvent e){
        System.out.println("release");
        super.mouseReleased(e);
        // done with the changing
   }

   public void mousePressed(java.awt.event.MouseEvent e){
      if (!this.contains(e.getPoint()))
	 	return;

   }

   
    public void paint(Graphics2D g){
		super.paint(g);
		_name.paint(g);
    }

*/
}

