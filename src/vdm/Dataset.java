package vdm;

import javax.swing.*;
import java.io.*;
import java.util.HashMap;
import java.util.HashSet;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Created by kekechen on 6/18/2014.
 */
public class Dataset {
    int _normLower = -1;
    int _normUpper = 1;

    int _rows, _dim, _arows, _adim; // active rows and cols

    float [][]  _norm_data, _init_data, _final_data;

    Vector<String>[] _cat;
    float [] _max, _min, _mean, _std;

    String [] _origRows;
    String [] _colNames;
    String [] _cLabel;
    String _filename;
    boolean _max_min_norm;

//    private final static Dataset _instance = new Dataset();
    public Dataset () {
        _origRows=null;
        _norm_data=null;
        _init_data=null;
    }
/*
    public static Dataset getInstance(){
        return _instance;
    }
*/
    public String getOrigRecord(int i){
        return _origRows[i];
    }
    public RecordSet load(Mapping mp) {
        JFileChooser fc = new JFileChooser();
        int returnVal = fc.showOpenDialog(null);
        File f = null;
        if (returnVal != JFileChooser.APPROVE_OPTION) {
            return null;
        }
        f = fc.getSelectedFile();
        _filename = f.getAbsolutePath();
        int n = JOptionPane.showConfirmDialog(
                null,
                "The first line of the file has the column name?",
                "Column Name",
                JOptionPane.YES_NO_OPTION);
        boolean hasColumnName = false;
        if (n == JOptionPane.YES_OPTION)
            hasColumnName = true;

        Object[] options = {"First Column",
                "Last Column",
                "No Label"};
        n = JOptionPane.showOptionDialog(null,
                "The dataset contains clustering labels at",
                "Clustering Labels",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[2]);

        int label_col = 0;
        if (n == JOptionPane.YES_OPTION) // first column
            label_col = 1;
        else if (n == JOptionPane.NO_OPTION) // last column
            label_col = -1;
        else
            label_col = 0; // no label

        Object[] norms = {"Standard", "Max-min"};
        n = JOptionPane.showOptionDialog(
                null,
                "Choose Normalization Method",
                "Normalization",
                JOptionPane.YES_NO_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                norms,
                norms[0]);
        _max_min_norm  = false;
        if (n == JOptionPane.NO_OPTION)
            _max_min_norm = true;

        return load2(label_col, hasColumnName, f, mp);
    }

    public RecordSet load2(int label_col, boolean hasColumnName, File f, Mapping mp){
        int nr=0;
        BufferedReader input;
        try {
            FileReader inputFS = new FileReader(f);
            input = new BufferedReader(inputFS);
        } catch (Exception e) {
            System.err.println("Can not open file");
            JOptionPane.showMessageDialog(null, "Cannot open file.");
            return null;
        }

        try{
            String line=input.readLine();
            //System.out.println(line);
            StringTokenizer tok1 = new StringTokenizer(line, "\t\n\r, ");
            _dim= tok1.countTokens();
            _colNames = new String [_dim];
            if (hasColumnName){
                for (int i=0; i<_dim; i++)
                    _colNames[i] = tok1.nextToken();
            }else {
                for (int i = 0; i < _dim; i++)
                    _colNames[i] = String.valueOf(i);
                nr=1;
            }
        }catch(Exception e){
            System.err.println("Error in reading files");
            return null;
        }

        String s;

        while (true) {
            try{

                if ((s=input.readLine())==null) {
                    _rows=nr;
                    break;
                } // end of if ()
                else
                if (!s.equals(""))
                    nr++;
            }catch(Exception e){
            }
        } // end of while ()

        _origRows = new String[_rows];
        _init_data=new float[_rows][_dim];


        try{
            input.close();
            FileReader inputFS = new FileReader(f);
            input = new BufferedReader(inputFS);
        }
        catch(Exception e){
            System.err.println("Can not open file");
            JOptionPane.showMessageDialog(null, "Cannot open file.");
            return null;
        }

        return normalize(label_col, input, hasColumnName, mp);

    }


    public String getFilename(){
        return _filename;
    }

    public int getDim(){
        return _dim;
    }
    public void setNormBounds(int upper, int lower)
    {
        _normUpper = upper;
        _normLower = lower;
    }
/*
    public double getUpperBound()
    {
        return _normUpper;
    }

    public double getLowerBound()
    {
        return _normLower;
    }
*/
    public String [] getColumnNames(){
        return _colNames;
    }

    public Vector<String> isCatAttribute(int dim){
        return _cat[dim];
    }

    public float getUpperBound(int dim){
        return _max[dim];
    }

    public float getLowerBound(int dim){
        return _min[dim];
    }

    public RecordSet normalize(int label, BufferedReader input, boolean hasColNames, Mapping mp){
        // label: 0  -- no label
        // 1: label at col 0
        // -1: label at last col


        if ( label!=0) {
            _cLabel = new String[_rows];
            _dim --;
        } // end of if ()

        _max=new float[_dim];
        _min=new float [_dim];
        _norm_data = new float[_rows][_dim];
        _final_data = _norm_data;

        _cat=new Vector [_dim];

        int i,j;


        for ( i=0; i<_dim;i++) {
            _max[i]=Float.MIN_VALUE;
            _min[i]=Float.MAX_VALUE;
            _cat[i]=null;
        } // end of for


        StringTokenizer tok;
        String linbit, srow;

        int nr=0;
        boolean flag= hasColNames;
        while (true) {
            try{
                String s;
                if ((s=input.readLine())==null) {
                    break;
                } // end of if ()
                else {
                    if (flag&&nr==0){
                        flag=false;
                        continue;
                    }
                   _origRows[nr] = s;
                    //System.out.println(s);
                    tok=new StringTokenizer(s,"\t\n\r, ");
                    if ( label == 1)
                        _cLabel[nr] = tok.nextToken(); // ignore first one, if it's label


                    for ( j =0; j<_dim&&tok.hasMoreElements(); j++) {
                        linbit= tok.nextToken();

                        try{
                            _init_data[nr][j]=Float.parseFloat(linbit);

                            if (_min[j]>_init_data[nr][j]) {
                                _min[j]=_init_data[nr][j];
                            } // end of if ()

                            if ( _max[j]<_init_data[nr][j]) {
                                _max[j]=_init_data[nr][j];
                            } // end of if ()
                            ///System.out.print(String.valueOf(_init_data[nr][j])+",");
                        }catch(Exception e){
                            // this is a category attribute
                            if (_cat[j]==null)
                                _cat[j]=new Vector<String>();
                            _cat[j].add(linbit); // add to the end of vector
                            _init_data[nr][j]=0;
                        }

                    }
                    if ( label == -1 ) {
                        if (!tok.hasMoreElements())
                            System.err.println("something is wrong");
                        _cLabel[nr]=tok.nextToken(); // the last one is label
                        //System.out.println(_cLabel[nr]);
                    } // end of if ()

                    nr++;
                }
            }catch(Exception e){
            }
        } // end of while ()


        // convert categorical values to numeric
        String tmpstr;

        int k,m;
        java.util.HashSet hs=new java.util.HashSet();

        for ( j=0; j<_dim; j++) {
            if (_cat[j]!=null) {
                String [] strs = (String []) _cat[j].toArray();
                int c = convert(strs, _init_data[j], 0);
                _min[j]=0;
                _max[j] = c-1;

            } // end of if ()

        } // end of for ()


        if (label !=0) {

            float [] labs = new float [_rows];
            convert(_cLabel, labs, 1);
            // output
            BufferedWriter output;
            try{
                FileWriter outputFS = new FileWriter(_filename+".label0");
                output = new BufferedWriter(outputFS);
                for (i=0; i<labs.length;i++){
                    output.write(String.valueOf((int)labs[i])+"\n");
                }
                output.close();
                outputFS.close();
            }
            catch(Exception e){
                JOptionPane.showMessageDialog(null, "Cannot open file.");
            }

        } // end of if ()

        /* max-min normalization */
        if (_max_min_norm) {
            float[] invtemp = new float[_dim];

            for (j = 0; j < _dim; j++) {

                if (_max[j] - _min[j] == 0)
                    invtemp[j] = 0;
                else
                    invtemp[j] = (float) 1.0 / (_max[j] - _min[j]);

                for (i = 0; i < _rows; i++) {
                    //_norm_data[i][j]= (float)((_init_data[i][j]-_min[j])*invtemp[j]); // normalized to [0, 1]
                    _norm_data[i][j] = (float) ((_init_data[i][j] - _min[j]) * (_normUpper - _normLower) * invtemp[j] + _normLower); // normalized to [-1, 1]
                } // end of for ()
            } // end of for ()
        }
        else {
            _mean = new float[_dim];
            _std = new float[_dim];
            for (j = 0; j < _dim; j++) {
                _mean[j] = 0;
                _std[j] = 0;
                for (i = 0; i < _rows; i++) {
                    _mean[j] += _init_data[i][j];
                }
                _mean[j] /= _rows;
                for (i = 0; i < _rows; i++) {
                    float t = _init_data[i][j] - _mean[j];
                    _std[j] += t * t;
                }
                _std[j] = (float) Math.sqrt((double) (_std[j] / _rows));
                if (_std[j] != 0) {
                    for (i = 0; i < _rows; i++) {
                        _norm_data[i][j] = (float) (_init_data[i][j] - _mean[j]) / _std[j];
                    }
                } else {
                    for (i = 0; i < _rows; i++)
                        _norm_data[i][j] = 0;
                }
            }
        }

        RecordSet rs=new RecordSet(0);
        for (i=0; i<_rows; i++){
            rs.add( new Record(i, _dim, _norm_data[i], mp));
        }
        System.out.println("load dataset: "+_dim+ " dimensions, "+_rows +" rows");
        return rs;
    }

    private int convert(String[] orig, float [] vals, int start){
        // convert categorical data to numeric
        java.util.HashSet hs=new java.util.HashSet();
        for (String s: orig) {
            hs.add(s);
            //System.out.println(s);
        }
        HashMap<String, Integer> hm=new HashMap<String, Integer>();
        int c=start;
        //System.out.println(hs);
        Object [] ss = hs.toArray();
        for (Object s: ss){
            hm.put((String) s, c);
            c++;
        }
        for (int i=0; i<orig.length; i++) {
            Integer v = hm.get(orig[i]);
            vals[i] = v;
        }

        return c;
    }
}
