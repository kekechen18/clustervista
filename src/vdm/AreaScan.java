package vdm;

import java.awt.*;
import java.util.Vector;

/**
 * Created by kekechen on 6/19/2014.
 */

public class AreaScan{
    class Entry{
        public int y_top;
        public int delta_y;
        public float x_int;
        public float x_change_per_scan;

        public void copy(Entry e){
            y_top=e.y_top;
            delta_y=e.delta_y;
            x_int=e.x_int;
            x_change_per_scan=e.x_change_per_scan;
        }
    }

    int[] x,y;
    int side_count, first_s, last_s, bottomscan, x_int_count;
    Entry[] sides;

    //int[] _cx, _cy, _selx, _sely;

    int  _totalY, _minY;

    int [] _selItem, _unselItem;
    int _numOfSel, _numOfUnsel;

    int _areaId;
    RecordSet _rs;

    public AreaScan(java.util.Vector<Point> points, RecordSet rs)
    {
        _rs = rs;
        side_count=points.size();
        first_s=0;
        last_s=0;

        x=new int[side_count];
        y=new int[side_count];
        sides=new Entry[side_count];

        for (int i=0;i<side_count; i++) {
            x[i]=(int)((Point)points.get(i)).getX();
            y[i]=(int)((Point)points.get(i)).getY();
            sides[i] = new Entry();
        } // end of for ()

    }


    public void pickSubset(RecordSet s){

        sort_on_bigger_y(side_count);
        //System.out.println("sort_on");

        _rs.sortOnY();
        _rs.reset();

        _unselItem = new int[_rs.size()];
        _selItem = new int[_rs.size()];
        _numOfUnsel =0;
        _numOfSel=0;
        int scan, i, j, k, c;

        for ( scan=sides[0].y_top; scan > bottomscan-1; scan--) {


            while (!_rs.atEnd()&&_rs.getYv()>scan){
                _unselItem[_numOfUnsel++]=_rs.getIndex();
                _rs.next();
            }

            update_first_and_last(scan);
            process_x_intersections(scan);
            draw_lines(scan, first_s);
            update_sides_list();
        } // end of for ()


        while (!_rs.atEnd()){
            _unselItem[_numOfUnsel++]=_rs.getIndex();
            _rs.next();
        }

        Vector<Integer> idx=new Vector<Integer>();
        for (i=0; i<_numOfSel; i++){
            idx.add(_selItem[i]);
        }

        _rs.fetch(idx, s);
    }


    private void put_in_sides_list(int entry, int x1, int y1,int x2,
                                   int y2, int next_y){
        int maxy;
        float x2_temp, x_change_temp;

        x_change_temp=(float)(x2-x1)/(float)(y2-y1);

        x2_temp=x2;

        if ((y2>y1)&& (y2<=next_y)) {
            y2--;
            x2_temp-=x_change_temp;
        } // end of if ()
        else {
            if ((y2<y1)&&(y2>=next_y)) {
                y2++;
                x2_temp+=x_change_temp;
            } // end of if ()

        } // end of else

        maxy=(y1>y2)?y1:y2;

        while ((entry>0)&&(maxy>sides[entry-1].y_top)) {
            sides[entry].copy(sides[entry-1]);
            entry--;
        } // end of while ()

        sides[entry].y_top = maxy;
        sides[entry].delta_y = Math.abs(y2-y1) + 1;

        if (y1>y2)
            sides[entry].x_int = x1;
        else
            sides[entry].x_int=x2_temp;

        sides[entry].x_change_per_scan = x_change_temp;
    }


    private void sort_on_bigger_y(int n){
        int k, x1, y1;

        int side=0;

        y1= y[n-1];
        x1= x[n-1];
        bottomscan=y[n-1];

        for ( k=0; k<n; k++) {

            if (y1!= y[k]) {
                put_in_sides_list(side, x1, y1, x[k],y[k], k==n-1?
                        y[0]: y[k+1]);
                side++;
            } // end of if ()

            if (y[k]<bottomscan) {
                bottomscan = y[k];
            } // end of if ()

            y1=y[k];
            x1=x[k];

        } // end of for ()

        side_count=side;

    }

    private   void update_first_and_last(int scan){

        while ( (sides[last_s+1].y_top>=scan) && (last_s< side_count))
            last_s++;

        while (sides[first_s].delta_y==0)
            first_s++;

    }

    public void swap(Entry x, Entry y){
        int i_temp;
        float f_temp;

        i_temp = x.y_top;
        x.y_top = y.y_top;
        y.y_top = i_temp;

        f_temp = x.x_int;
        x.x_int=y.x_int;
        y.x_int=f_temp;

        i_temp = x.delta_y;
        x.delta_y = y.delta_y;
        y.delta_y = i_temp;

        f_temp = x.x_change_per_scan;
        x.x_change_per_scan=y.x_change_per_scan;
        y.x_change_per_scan=f_temp;
    }

    private void sort_on_x (int entry, int first_s){

        while ( ( entry >first_s) && (sides[entry].x_int < sides[entry-1].x_int)) {
            swap(sides[entry], sides[entry-1]);
            entry--;
        } // end of while ()
    }


    private void process_x_intersections(int scan){
        int k;

        x_int_count=0;

        for (k = first_s; k< last_s+1; k++) {
            if (sides[k].delta_y > 0) {
                x_int_count++;
                sort_on_x(k,first_s);
            } // end of if ()

        } // end of for ()

    }

    private void update_sides_list(){
        int k;

        for (k = first_s; k<last_s+1; k++) {
            if ( sides[k].delta_y>0) {

                sides[k].delta_y --;
                sides[k].x_int -= sides[k].x_change_per_scan;

            } // end of if ()

        } // end of for ()


    }

    private void draw_lines(int scan, int index){
        int k, x, x1=0, x2=0, size,i, j, c;

        for ( k=0 ; k< (int) ( x_int_count/2+0.5); k++) {
            while (sides[index].delta_y==0) {
                index++;
            } // end of while ()

            x1=(int)(sides[index].x_int+0.5);

            index++;

            while (sides[index].delta_y==0 ) {
                index++;
            } // end of while ()

            x2 = (int )( sides[index].x_int +0.5);

            while (!_rs.atEnd()&&_rs.getYv()==scan&&_rs.getXv()<x1) {
                _unselItem[_numOfUnsel++]=_rs.getIndex();
                _rs.next();
            } // end of while ()

            while (!_rs.atEnd()&&_rs.getYv()==scan&&_rs.getXv()<=x2) {
                _selItem[_numOfSel++]=_rs.getIndex();
                _rs.next();
            } // end of while ()

            //g.drawLine(x1, scan, x2, scan);

            index++;
        } // end of for (


        while (!_rs.atEnd()&&_rs.getYv()==scan&&_rs.getXv()>x2) {
            _unselItem[_numOfUnsel++]=_rs.getIndex();
            _rs.next();
        } // end of while ()

    }

}
