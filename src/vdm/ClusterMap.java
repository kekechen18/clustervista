
/**
 * Created by kekechen on 6/23/2014.
 */
package vdm;

import java.awt.*;
import java.util.HashMap;
import java.util.Vector;

public class ClusterMap {
    private static final double DECAY = 0.5;
    private static final ClusterMap INSTANCE = new ClusterMap();
    HashMap<Integer, Vector<Integer>> _map;
    Dimension _size;
    double [][]_dense;
    double [][] _filter;
    double _maxDense;
    private ClusterMap() {
        _map = new HashMap<Integer, Vector<Integer>>();
        _filter = new double[5][];
        for (int i=0; i<5; i++){
            _filter[i] = new double[5];
        }
        double d2 = DECAY*DECAY;
        for (int i=0; i<5; i++)
            for (int j=0; j<5; j++){
                if (i==2&&j==2)
                    _filter[i][j] = 1;
                if ((i==2||i==3)&&(j==2|| j==3))
                    _filter[i][j] = DECAY;
                if ((i==0||i==4)&&(j==0|| j==4))
                    _filter[i][j] = d2;

            }
    }

    public void setSize(Dimension d){
        _size = d;
        clear();
    }
    public void clear(){
        //System.out.println("CM is cleared");
        _map.clear();
        _dense = new double[(int) _size.getHeight()][];
        for (int i=0; i<_size.getHeight(); i++)
            _dense[i] = new double[(int) _size.getWidth()];
        _maxDense =0;
    }
    public void put(int x, int y, int i){
        Vector<Integer> v;
        int idx = x*10000+y;
        if (_map.containsKey(idx)){
            v = _map.get(idx);
        }else{
            v = new Vector<Integer>();
            _map.put(idx, v);
        }
        v.add(i);
        addDenseMap(x,y);
        //System.out.println("put ("+String.valueOf(x)+" "+String.valueOf(y)+")"+v);
    }

    public void addDenseMap(int x, int y){
        int x0, y0;
        for (int i=0; i<5; i++)
            for (int j=0; j<5; j++){
                x0=x-2-i;
                y0=y-2-i;
                _dense[x0][y0]+= _filter[i][j];
                if (_dense[x0][y0]>_maxDense){
                    _maxDense = _dense[x0][y0];
                }

            }
    }

    public void draw(Graphics g){

    }

    public Vector <Integer> get(int x0, int y0){
        Vector<Integer> v= new Vector<Integer>();
        int delta =10;
        for (int x=x0-delta; x<=x0+delta; x++) {
            for (int y = y0 - delta; y < y0 + delta; y++) {
                int idx = x * 10000 + y;
                Vector t = _map.get(idx);
                //System.out.println("get ("+String.valueOf(x)+" "+String.valueOf(y)+")"+t);
                if (t!=null)
                    v.addAll(t);
            }
        }

        return v;
    }
    public static ClusterMap getInstance() {
        return INSTANCE;
    }


}